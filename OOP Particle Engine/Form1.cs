﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace OOP_Particle_Engine
{
    public partial class Form1 : Form
    {
        //Much like a variable, create/allocate memory for the particleEngine class
        ParticleEngine particleEngine;

        //Variables to hold the positions of the mouse
        int mouseX;
        int mouseY;
        public Form1()
        {
            particleEngine = new ParticleEngine(new Point(100, 100)); //Initialize the particleEngine class
            InitializeComponent();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            //Enable the spawn timer
            particleSpawn.Enabled = true;
            mouseX = e.X;
            mouseY = e.Y;
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            //Turn off spawning of particles
            particleSpawn.Enabled = false;
        }

        private void particleTimer_Tick(object sender, EventArgs e)
        {
          //particleEngines method that updates the particles positions and total lifeSpan
            particleEngine.Update();
           
            //Redraw scene
            this.Refresh();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            //Handles drawing of all particles
            particleEngine.Draw(e);
        }

        private void particleSpawn_Tick(object sender, EventArgs e)
        {
            //emitterLocation is the variable in our particleEngine object that controls where particles spawn (currently set to mouse position)
            particleEngine.emitterLocation = new Point(mouseX, mouseY);
            particleEngine.Spawn(5,10);//Currently set to spawn 5 pixals a tick at a speed of 10
            
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            //Update mouse variable positions
            mouseX = e.X;
            mouseY = e.Y;
        }

    

       
    }
}
