﻿using System;
using System.Collections.Generic;
//Include drawing libraires
using System.Drawing; //Allows some basic GUI drawings
using System.Windows.Forms;

namespace OOP_Particle_Engine
{
    class ParticleEngine
    {
        #region instance variables
        private Random random; //Will be used to spawn random elements of the particles
        public Point emitterLocation { get; set; } //Location of spawn
        private List<Particle> particles; //List to hold spawned particles
        private int speed; //Variable to hold the BASE speed of all particles
       
        #endregion
        #region Constructors
        public ParticleEngine(Point location)
        {
            //Set location of spawn
            emitterLocation = location;
            this.particles = new List<Particle>();
            random = new Random();
        
        }
        
        

        private Particle GenerateNewParticle()
        {//This is where we dynamically create a single particle

            
            
            //Spawn at defined location
            Point position = emitterLocation;

            //Generate a random velocity based upon speed variable
            Point velocity = new Point(
                   (int)(1f * (float)(random.NextDouble() * speed - (speed / 2))),
                   (int)( 1f * (float)(random.NextDouble() * speed - (speed / 2))));


            //Generate max time onscreen
            int lifeSpan = 10 + random.Next(30);

            //Random size of particle
            int randSize = random.Next(3, 7);

            //Try incorperating colour into our particle object.

            //Return the sum of these random variables as 1 new particle
            return new Particle( position, velocity, lifeSpan,randSize,randSize);
    
        }



        #endregion

        #region Methods
        public void Spawn(int ammount, int speed)
        {
            //Determines how many particles will spawn at each update
            int total = ammount;
            this.speed = speed;
            for (int i = 0; i < total; i++)
            {
               
                particles.Add(GenerateNewParticle()); //Add the return value of the function "GenerateNewParticle" to our list.
                
            }
        }


        public void Update()
        {
         
            //Update movement of all visable particles
            for (int particle = 0; particle < particles.Count; particle++)
            {
                particles[particle].Update();
                if (particles[particle].lifeSpan <= 0)
                {//If the particles lifeSpan reaches 0 remove it from the list.
                    particles.RemoveAt(particle);
                    particle--; //Make sure to watch for fence post errors
                   
                }
            }
        }

        public void Draw(PaintEventArgs e)
        {
          
            //Draw all particles to screen
            for (int i = 0; i < particles.Count; i++)
            {
                particles[i].Draw(e);
            }

        }
        #endregion

    }
}
