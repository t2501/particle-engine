﻿using System;
//Removing the libraries you do not need is also good practice

//Include drawing libraires
using System.Drawing; //Allows some basic GUI drawings
using System.Drawing.Drawing2D; //An extension of the drawing framework in case you may need it
using System.Drawing.Imaging;
using System.Windows.Forms;



namespace OOP_Particle_Engine
{
    //This class handles single particles
    class Particle
    {
        /* Instance Variables are what the object knows about itself. In other words each object has unique values
         * such as its colour, size and speed.
         * When creating an object think of what the object may possess ex: A sword object will have - attackSpeed,damage,length variable  */

        #region Instance variables
        /// <summary>
        /// Public: allows access from diffrent classes
        /// {get; set;}: allows us to change the value of the variable or get a returned value when called upon
        /// </summary>
        public Image Image { get; set; }            //The texture that will be drawn to represent the particle
        public Point Position { get; set; }        //The current position of the particle        
        public Point Velocity { get; set; }        //The speed and direction of the particle
        public int lifeSpan { get; set; }          //The length of time the particle will remain visable
        public int Width { get; set; }             //Height and Width of the particle
        public int Height { get; set; }  
       
       //Try thinking of other variables that a particle may need.

        #endregion

        #region Constructors
        /* A Constructor is essentially where you assign values to the above instance variables. The particle Engine assinges
         * random values to all these variables by calling on this method */

        public Particle( Point position, Point velocity, int lifeSpan,int width, int height)
        {
            // Fill in our instance variables
            
            Position = position;
            Velocity = velocity;
            this.lifeSpan = lifeSpan;
            this.Height = height;
            this.Width = width;
            Image = new Bitmap(width, height);
            

           
            
        }
        #endregion

        #region methods
        /* methods are the engine to our object, they are how our object behaves. */ 
        public void Update()
        {
            //Update the Particles position and life time

            //Incement (de-increment?) life time
            lifeSpan--; //Same as: lifeSpan = lifeSpan - 1;

            //Increment postion by velocity
            Position = new Point(Position.X + Velocity.X, Position.Y + Velocity.Y);


        }

        public void Draw(PaintEventArgs e)
        {
            //Hint* to add dynamic colour to our object we would need to change this static RED to a colour variable genereated from our particle engine.
            Brush b = new SolidBrush(Color.Red);

            Rectangle rect = new Rectangle(new Point(0,0), new Size(Width, Height));
                
            System.Drawing.Graphics g = Graphics.FromImage(Image);
            g.FillRectangle(b, rect);

            e.Graphics.DrawImage(this.Image, Position);
            g.Dispose();
        }
        #endregion
    }
}
