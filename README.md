# OOP Particle Engine

This project is an Object-Oriented Particle Engine that provides a framework to simulate particles in a graphical environment. It is built using C# and utilizes the `System.Drawing` namespace for rendering graphics. It was originally created to help students learn the aspects of OOP coding.

## Features

- **ParticleEngine:** 
  - Dynamically generate particles with randomized properties such as velocity, lifespan, size, etc.
  - Define the location of spawn (emitter location).
  - Update the movement and state of each particle.
  - Draw particles to the screen.

- **Particle:**
  - Defines individual particles.
  - Contains properties like Image, Position, Velocity, Lifespan, Width, and Height.
  - Provides methods for updating the particle's state and drawing it to the screen.

## Getting Started

1. **Prerequisites:** 
   - Ensure you have the appropriate .NET runtime installed.
   
2. **Setup:** 
   - Clone/download the repository.
   - Open the solution/project in your preferred C# IDE (e.g., Visual Studio).

3. **Running:** 
   - Build and run the project. 

## Usage

- Initialize the `ParticleEngine` by providing the emitter location.
  
  ```csharp
  ParticleEngine particleEngine = new ParticleEngine(new Point(x, y));
  particleEngine.Spawn(ammount, speed);
  particleEngine.Update();
  particleEngine.Draw(e);  // 'e' is of type PaintEventArgs.


